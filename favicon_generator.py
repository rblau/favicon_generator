#!/usr/bin/env python3

# Favicon Generator for new projects
#
# Author: Rudolf Unruh <unruh@blausoft.net>
# Created: 2019-18-01

import argparse
import subprocess

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('output', help='path to write to')
    parser.add_argument('--background-color', '-b', default='#007bff')
    parser.add_argument('--text-color', '-c', default='#ffffff')
    parser.add_argument('--text', '-t', default='x', help='1-3 letters')
    args = parser.parse_args()

    subprocess.call(['convert',
        '-background', args.background_color,
        '-fill', args.text_color,
        '-size', '192x192',
        '-gravity', 'center',
        'label:' + args.text,
        args.output
        ])

if __name__ == '__main__':
    main()
